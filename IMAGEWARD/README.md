## IMAGEWARD
-----
Generates a watermarked image for each of the passed images using it's own metadata

### Prerequisites
	- imagemagick
	- feh
	- hexdump
