#!/bin/bash
#
#  Adrián Estévez Barreiro 
#
# -------------------------------
#
# This script allows the user to watermark his images using its own metadata
#
# -------------------------------


PROGRAM=$(basename $0)
AUTHOR="Adrián Estévez Barreiro"
_IMG_LIST=()
_NEW_IMG_LIST=()
_image=$1
_verbose=false
_show=false
_test_img="./test.jpg"
_encrypt=false
_dir="."

trap _ctrl_c 2

_ctrl_c (){
	echo -e "\nExiting..."
	_exit 2
}

_exit(){
	exit ${1:-0}
}

_error(){
	_sig=${1:-1}

	shift
	echo "$*"

	_exit $_sig
}

_help(){
	
	_verbose=true
	_print << EOF
Usage: $PROGRAM [options] img_list
Options:
	-a	Set images author
	-c 	Convert unprotected images to given size. If none then dafault is used.
	-e	Encrypts file name
	-h	Shows this message
	-i  Set input directory or file
	-o	Set output directory
	-s	Shows warded images at the end
	-v	Be verbose
EOF
	_exit $1
}

#Waits for a process to end 
_counter(){
	while ps $1 > /dev/null; do echo -n "."; sleep 1; done; echo
}

_get_metadata(){


	echo -n "${1}: "
	echo -n "Fetching metadata"

	TMPF="$(mktemp)"

	#Get METADATA in background
	(identify -verbose $1 > $TMPF) &

	#WAIT FOR PROCESS TO END AND RECOVER METADATA
	_counter $!
	METADATA=$(cat $TMPF)

	#GET DATA
	_camera="$(awk -F : 'BEGIN {out="unkown"}; /exif:Model/ {out=$NF}; END {print out}' <<< $METADATA | sed 's/^ *//' )"
	_lens="$(awk -F : 'BEGIN {out="unkown"}; /exif:LensModel/ {out=$NF}; END {print out}' <<< $METADATA | sed 's/^ *//' )"
	#_lens="$(awk -F  ' ' '/exif:LensModel/ {print $2}' <<< $METADATA | sed 's/^ *//' )"
	_aperture="f/$(bc -l <<< $(awk -F : '/exif:FNumber/ {print $NF}' <<< $METADATA ) | sed 's/\.*0\+$//')"
	_exposure="$(awk -F : '/exif:ExposureTime/ {print $NF}' <<< $METADATA | sed 's/^ *//')s"
	_length="$(awk -F : '/exif:FocalLength/ {print $NF}' <<< $METADATA | sed 's/^ *\([^/]*\).*/\1/')mm"
	_ISO=$(awk -F : '/exif:Photog/ {print $NF}' <<< $METADATA | sed 's/^ *//' )
	_size=$(awk -F : '/exif:PixelY/ {print $NF}' <<< $METADATA | sed 's/^ *//' )
}

_echo(){
	if ${_verbose} ; then
		echo -e $*
	fi
}

_print(){
	if ${_verbose} ; then
		while read data; do
			echo -e ${data}
		done
	fi
}

_show_info(){

_print <<-EOF
	IMAGE DATA FOR ${_image}:
	-------------------------
	AUTHOR: $AUTHOR
	CAMERA: $_camera
	LENS: $_lens

	APERTURE: $_aperture
	FOCAL LENGTH: $_length
	EXPOSURE TIME: $_exposure
	ISO: $_ISO
	-------------------------

EOF

}

_ward_image(){

	#new_image="${_image%.*}.warded.${_image##*.}"
	new_image="${2}.warded.${1##*.}"
	_image="${1}"
	_NEW_IMG_LIST+=(${new_image})

	_ps="$(bc <<< ${_size}*0.015)"
	_ps=${_ps%.*}
	_offset="$(( _ps * 4))"

	convert -font Barlow-Regular -pointsize ${_ps} -fill white -gravity southwest \
		-draw "text ${_offset},$((_offset+_ps)) '${AUTHOR}'" \
		-draw "text ${_offset},$((_offset)) '${_camera}'" \
		-draw "text ${_offset},$((_offset-_ps)) '${_lens}'" \
		-gravity southeast \
		-draw "text ${_offset},$((_offset+_ps)) '${_aperture}'" \
		-draw "text ${_offset},$((_offset)) '${_length}'" \
		-draw "text ${_offset},$((_offset-_ps)) '${_exposure}'" \
		${_image} ${new_image} && echo -e "Generated image ${new_image}\n" || _error $? "${_image}"


}

_show_images(){
	_verbose=true
	_print <<-EOF
		-------------------------
		LIST OF GENERATED IMAGES 
		Total: ${#_NEW_IMG_LIST[@]}
		-------------------------
	EOF
	_verbose=false

	for _i in $(seq 0 $((${#_NEW_IMG_LIST[@]}-1))); do
		echo "${_IMG_LIST[${_i}]} -> ${_NEW_IMG_LIST[${_i}]}"
	done

	if ${_show}; then feh -. ${_NEW_IMG_LIST[@]}; fi
}
_main(){
	
	echo ALL IMAGES: $*
	for _image in $*; do
		_pre="${_dir}/${_image##*\/}"
		echo ${_pre}
		_pre="${_pre%.*}"
		echo ${_pre}
		_small=""

		#if convert -> convert
		if [[ -n ${NSIZE} ]]; then
			_small="${_pre}.small.${_image##*.}"
			_echo "convert ${_image} -resize x${NSIZE} ${_small}"
			convert ${_image} -resize x${NSIZE} ${_small}
		fi

		#if encrypt -> encrypt
		if ${_encrypt}; then
			_pre="${_dir}/$(hexdump -n16 -e '1 "%08x"' /dev/urandom)"
			if [[ ! -z ${_small} ]]; then
				_echo "mv ${_small} ${_pre}.hex.${_image##*.}"
				mv ${_small} ${_pre}.hex.${_image##*.}
			else
				_echo "cp ${_image} ${_pre}.hex.${_image##*.}"
				cp ${_image} ${_pre}.hex.${_image##*.}
			fi
		fi

		_get_metadata ${_image}
		_show_info 
		_ward_image ${_image} ${_pre}
		
	done

	_show_images

}

#### EXECUTION
if [ $# -eq 0 ]; then _help 1; fi

# While there are arguments
while [ $# -gt 0 ]; do
	case $1 in
		-a) AUTHOR="$2"; shift
			;;
		-c) if [[ "$2" =~ ^[[:digit:]]$ ]]; then
				#If it's a digit set NSIZE
				NSIZE="$2"; 
				shift
			else
				#Else set to default 1080p
				NSIZE=1080
			fi
			;;
		-e) _encrypt=true
			;;
		-h) _help
			;;
		-i) if [[ -d $2 ]]; then
				#If directory exists find all files and add them
				_IMG_LIST+=("$(find $2 -maxdepth 1 -type f -name "[^.]*")")
				shift
			fi 
			;;
		-o) if [[ ! -d $2 ]]; then 
				_error 1 "$1 must be followed by an existing directory"
			fi
			_dir=$(sed 's/\/$//' <<< $2)
			shift
			;;
		-s) _show=true
			;;
		-v) _verbose=true
			;;
		-*) echo "Not recognized option"
			_help 1
			;;
		test:)	_IMG_LIST+=("${_test_img}")
			;;
		*)	if [[ -d $1 ]]; then
				echo "Skipping directory $1"
				shift
			elif [[ ! -e $1 ]]; then
				_error 1 "File $1 does not exist"
			elif [[ ! -r $1 ]]; then
				_error 1 "File $1 is not readeable"
			else
				_IMG_LIST+=("$1");
			fi
			;;
	esac

	#Move to next argument
	shift 
done


if [[ -z ${_IMG_LIST} ]]; then
	_error 1 "No images detected"
fi

_main ${_IMG_LIST[@]}

